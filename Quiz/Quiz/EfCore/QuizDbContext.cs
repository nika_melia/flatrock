﻿using Microsoft.EntityFrameworkCore;
using Quiz.Core;

namespace Quiz.EfCore
{
    public class QuizDbContext : DbContext
    {
        public QuizDbContext(DbContextOptions<QuizDbContext> options) : base(options)
        {
        }

        public DbSet<Question> Questions { get; set; }
    }
}
