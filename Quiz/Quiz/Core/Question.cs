﻿namespace Quiz.Core
{
    public class Question
    {
        public int Id { get; set; }
        public string Quote { get; set; }
        public string Author { get; set; }
    }
}
