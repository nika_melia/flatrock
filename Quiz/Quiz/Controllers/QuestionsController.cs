﻿using Microsoft.AspNetCore.Mvc;
using Quiz.Application;
using Quiz.Controllers.Models;
using Quiz.Core;

[Route("api/[controller]")]
[ApiController]
public class QuestionsController : ControllerBase
{
    private readonly IQuestionService _questionService;

    public QuestionsController(IQuestionService questionService)
    {
        _questionService = questionService;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Question>>> GetQuestions()
    {
        return Ok(await _questionService.GetAllQuestionsAsync());
    }

    [HttpGet("GetRandomQuestion/{mode}/{id}")]
    public async Task<ActionResult<QuestionModel>> GetRandomQuestionById(int id, string mode)
    {
        return Ok(await _questionService.GetRandomQuestion(id, mode));
    }

    [HttpGet("GetRandomQuestion/{mode}")]
    public async Task<ActionResult<QuestionModel>> GetRandomQuesrion(string mode)
    {
        return Ok(await _questionService.GetRandomQuestion(mode));
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Question>> GetQuestion(int id)
    {
        var question = await _questionService.GetQuestionByIdAsync(id);
        if (question == null)
        {
            return NotFound();
        }
        return question;
    }

    [HttpPost]
    public async Task<ActionResult<Question>> PostQuestion(Question question)
    {
        var createdQuestion = await _questionService.CreateQuestionAsync(question);
        return CreatedAtAction("GetQuestion", new { id = createdQuestion.Id }, createdQuestion);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteQuestion(int id)
    {
        await _questionService.DeleteQuestionAsync(id);
        return NoContent();
    }
}
