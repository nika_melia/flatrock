﻿using Quiz.Core;

namespace Quiz.Controllers.Models
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public string Quote { get; set; }
        public string Author { get; set; }
        public List<string> Options { get; set; }
        public string CorrectAnswer { get; set; }
    }
}
