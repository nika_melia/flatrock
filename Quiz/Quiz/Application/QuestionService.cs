﻿using Microsoft.EntityFrameworkCore;
using Quiz.Controllers.Models;
using Quiz.Core;
using Quiz.EfCore;

namespace Quiz.Application
{
    public class QuestionService : IQuestionService
    {
        private readonly QuizDbContext _context;
        private Random _random = new Random();


        public QuestionService(QuizDbContext context)
        {
            _context = context;
            _random = new Random();
        }

        public async Task<IEnumerable<Question>> GetAllQuestionsAsync()
        {
            return await _context.Questions.ToListAsync();
        }

        public async Task<Question?> GetQuestionByIdAsync(int id)
        {
            return await _context.Questions.FirstOrDefaultAsync(q => q.Id == id);
        }

        public async Task<Question> CreateQuestionAsync(Question question)
        {
            _context.Questions.Add(question);
            await _context.SaveChangesAsync();
            return question;
        }

        public async Task DeleteQuestionAsync(int id)
        {
            var question = await _context.Questions.FindAsync(id);
            if (question != null)
            {
                _context.Questions.Remove(question);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<QuestionModel> GetRandomQuestion(string mode)
        {
            int count = _context.Questions.Count();
            int index = _random.Next(count);
            var question= await _context.Questions.Skip(index).FirstAsync();
            return await GetPossibleAnswers(question, mode);
        }

        public async Task<QuestionModel> GetRandomQuestion(int id, string mode)
        {
            int count = _context.Questions.Count(); 
            int index = _random.Next(count);
            var question = await _context.Questions.Where(q => q.Id != id).Skip(index).FirstAsync();
            return await GetPossibleAnswers(question, mode);

        }

        public async Task<QuestionModel> GetPossibleAnswers(Question question, string mode)
        {
            QuestionModel questionModel = new QuestionModel();
            questionModel.Author = question.Author;
            questionModel.Quote = question.Quote;
            questionModel.Id = question.Id;
            var possibleAuthors = await _context.Questions
                                                .Select(q => q.Author)
                                                .Distinct()
                                                .ToListAsync();
            possibleAuthors.Remove(question.Author);
            if (mode == "binary")
            {
                if(_random.Next()%2 == 0)
                {
                    questionModel.CorrectAnswer = "Yes";
                    questionModel.Options = new List<string> { question.Author };
                }
                else
                {
                    questionModel.CorrectAnswer = "No";
                    questionModel.Options = possibleAuthors.OrderBy(x => _random.Next()).Take(1).ToList();
                }
            }
            else
            {
                questionModel.CorrectAnswer = question.Author;
                questionModel.Options = possibleAuthors.OrderBy(x => _random.Next()).Take(3).ToList();
                questionModel.Options.Add(question.Author);
                questionModel.Options.OrderBy(x => _random.Next());
            }
            return questionModel;

        }
    }

}

