﻿using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Quiz.Application
{
    public static class Configuration
    {
        public static IServiceCollection AddQuizServices(this  IServiceCollection services)
        {
            services.AddScoped<IQuestionService, QuestionService>();
            return services;
        }
    }
}
