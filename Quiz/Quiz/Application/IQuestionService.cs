﻿using Microsoft.AspNetCore.Mvc;
using Quiz.Controllers.Models;
using Quiz.Core;

namespace Quiz.Application
{
    public interface IQuestionService
    {
        Task<IEnumerable<Question>> GetAllQuestionsAsync();
        Task<Question> GetQuestionByIdAsync(int id);
        Task<Question> CreateQuestionAsync(Question question);
        Task DeleteQuestionAsync(int id);
        Task<QuestionModel> GetRandomQuestion(int id, string mode);
        Task<QuestionModel> GetRandomQuestion(string mode);
    }
}
