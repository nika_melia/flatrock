import "./App.css";
import { Routes, Route } from "react-router-dom";
import Settings from "./components/settings/Settings";
import Nav from "./components/Nav/Nav";
import Home from "./components/main/homePage";
import { useState } from "react";
import QuestionsPage from "./components/question/QuestionsPage";

function App() {
  const [mode, setMode] = useState("binary");

  return (
    <div>
      <Nav />
      <Routes>
        <Route path="/" element={<Home mode={mode} />} />
        <Route path="settings" element={<Settings setMode={setMode} />} />
        <Route path="/questionsPage" element={<QuestionsPage />} />
      </Routes>
    </div>
  );
}

export default App;
