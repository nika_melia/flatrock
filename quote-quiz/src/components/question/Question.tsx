import React from "react";
import "./css/Question.css"; // Ensure the CSS file is correctly imported

interface QuestionProps {
  quote: string;
  author?: string; // Author is optional
  gameMode: string;
}

const Question: React.FC<QuestionProps> = ({ quote, author, gameMode }) => {
  return (
    <div className="question-container">
      {gameMode === "binary" && author ? (
        <p className="quote-text">
          Did this author {author} say this quote: "{quote}"?
        </p>
      ) : (
        <>
          <p className="quote-text">{quote}</p>
        </>
      )}
    </div>
  );
};

export default Question;
