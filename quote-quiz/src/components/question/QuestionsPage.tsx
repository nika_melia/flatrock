import React, { useState, useEffect } from "react";
import { fetchQuestion } from "../../services/QuestionService";
import { QuestionModel } from "../../services/models/QuestionsModel";
import QuestionsList from "./QuestionsList";
import AddQuestionForm from "./AddQuestionForm";

const QuestionsPage: React.FC = () => {
  const [questions, setQuestions] = useState<QuestionModel[]>([]);
  const [showForm, setShowForm] = useState(false);
  const handleAddButtonClick = () => {
    setShowForm(true); // Show the form when the button is clicked
  };
  useEffect(() => {
    const loadQuestions = async () => {
      const data = await fetchQuestion();
      setQuestions(data);
    };
    loadQuestions();
  }, []);
  return (
    <div>
      <QuestionsList questions={questions}></QuestionsList>
      <button onClick={handleAddButtonClick}>Add New Question</button>
      {showForm && (
        <AddQuestionForm
          setQuestions={setQuestions}
          setShowForm={setShowForm}
        />
      )}
    </div>
  );
};

export default QuestionsPage;
