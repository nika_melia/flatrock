// QuestionItem.tsx
import React from "react";
import { QuestionModel } from "../../services/models/QuestionsModel";
import "./css/QuestionsItem.css";

interface QuestionItemProps {
  question: QuestionModel;
  onClick: (question: QuestionModel) => void;
}

const QuestionItem: React.FC<QuestionItemProps> = ({ question, onClick }) => {
  return (
    <div className="question-item" onClick={() => onClick(question)}>
      <h4>{question.quote}</h4>
      <p>Author: {question.author}</p>
    </div>
  );
};

export default QuestionItem;
