// AddQuestionForm.tsx
import React, { useState } from "react";
import "../question/css/AddQuestionsForm.css";
import { addQuestion } from "../../services/QuestionService";
import { QuestionModel } from "../../services/models/QuestionsModel";

interface AddQuestionFormProps {
  setQuestions: React.Dispatch<React.SetStateAction<QuestionModel[]>>;
  setShowForm: React.Dispatch<React.SetStateAction<boolean>>;
}

const AddQuestionForm: React.FC<AddQuestionFormProps> = ({
  setQuestions,
  setShowForm,
}) => {
  const [questionText, setQuestionText] = useState("");
  const [author, setAuthor] = useState("");

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    const newQuestion = await addQuestion({
      id: 0,
      quote: questionText,
      author: author,
    });
    setQuestions((prevQuestions) => [...prevQuestions, newQuestion]);
    setShowForm(false); // Hide form after submission
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={questionText}
        onChange={(e) => setQuestionText(e.target.value)}
        placeholder="Enter question text"
        required
      />
      <input
        type="text"
        value={author}
        onChange={(e) => setAuthor(e.target.value)}
        placeholder="Author"
        required
      />
      <button type="submit">Submit</button>
      <button type="button" onClick={() => setShowForm(false)}>
        Cancel
      </button>
    </form>
  );
};

export default AddQuestionForm;
