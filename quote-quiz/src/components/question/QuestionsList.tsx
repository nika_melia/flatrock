// QuestionsList.tsx
import { deleteQuestion } from "../../services/QuestionService";
import { QuestionModel } from "../../services/models/QuestionsModel";
import QuestionItem from "./QuestionsItem";

interface QuestionsListProprs {
  questions: QuestionModel[];
}

const QuestionsList: React.FC<QuestionsListProprs> = ({ questions = [] }) => {
  const handleQuestionClick = (question: QuestionModel) => {
    // Ask user to confirm deletion
    if (window.confirm("Are you sure you want to delete this question?")) {
      deleteQuestion(question.id);
    }
  };

  return (
    <div>
      <h2>Questions List</h2>
      {questions.map((question) => (
        <QuestionItem
          key={question.id}
          question={question}
          onClick={() => handleQuestionClick(question)}
        />
      ))}
    </div>
  );
};

export default QuestionsList;
