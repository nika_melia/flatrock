import React from "react";
import Answer from "../answer/Answer";
import Question from "../question/Question";
import { useEffect, useState } from "react";
import {
  fetchNextQuestion,
  fetchNextQuestionById,
} from "../../services/QuestionService";
import { QuizQuestionModel } from "../../services/models/QuizQuestionModel";
interface HomeProps {
  mode: string;
}

const Home: React.FC<HomeProps> = ({ mode }) => {
  const onAnswer = (answer: string, author: string, correctAnswer: string) => {
    if (answer === correctAnswer) {
      alert(`Correct! The right answer is: ${author}`);
    } else {
      alert(`Sorry, you are wrong! The right answer is: ${author}`);
    }
  };

  const [quizQuestion, setQuestion] = useState<QuizQuestionModel | undefined>();

  useEffect(() => {
    if (quizQuestion) {
      console.log("aqac shemodis?");
      fetchNextQuestionById(quizQuestion.id, mode).then(setQuestion);
    } else {
      console.log("before");
      fetchNextQuestion(mode).then(setQuestion);
      console.log(quizQuestion);
    }
  }, []);

  const handleFetchQuestion = () => {
    if (quizQuestion) {
      console.log("aqac shemodis?");
      console.log(quizQuestion);
      fetchNextQuestionById(quizQuestion.id, mode).then(setQuestion);
    } else {
      console.log("before");
      fetchNextQuestion(mode).then(setQuestion);
      console.log(quizQuestion);
    }
  };
  return (
    <>
      <Question
        quote={quizQuestion?.quote}
        gameMode={mode}
        author={quizQuestion?.author}
      />
      <Answer
        mode={mode}
        author={quizQuestion?.author}
        onAnswer={onAnswer}
        options={quizQuestion?.options}
        correctAnswer={quizQuestion?.correctAnswer}
      />
      <button onClick={handleFetchQuestion}>Get New Question</button>
    </>
  );
};

export default Home;
