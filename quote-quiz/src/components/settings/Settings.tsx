import { useNavigate } from "react-router-dom";

interface SettingsProps {
  setMode: (mode: string) => void; // Receive setMode as a prop
}

const Settings: React.FC<SettingsProps> = ({ setMode }) => {
  const navigate = useNavigate();

  return (
    <div>
      <h1>Settings</h1>
      <button
        onClick={() => {
          setMode("binary");
          navigate("/");
        }}
      >
        Binary Mode
      </button>
      <button
        onClick={() => {
          setMode("multiple");
          navigate("/");
        }}
      >
        Multiple Choice Mode
      </button>
    </div>
  );
};

export default Settings;
