import { Link } from "react-router-dom";
import "./Nav.css";
const Nav = () => {
  return (
    <nav>
      <ul className="navbar">
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/settings">Settings</Link>
        </li>
        <li>
          <Link to="/questionsPage">Questions</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
