import React from "react";
import "./Answer.css"; // Ensure this path is correct

interface AnswerProps {
  mode: string;
  author: string;
  options: string[];
  correctAnswer: string;
  onAnswer: (answer: string, author: string, correctAnswer: string) => void;
}

const Answer: React.FC<AnswerProps> = ({
  mode,
  options,
  onAnswer,
  author,
  correctAnswer,
}) => {
  const renderOptions = () => {
    if (mode === "binary") {
      return (
        <div className="answer-binary">
          <button
            key="yes"
            className="btn yes-btn"
            onClick={() => onAnswer("Yes", author, correctAnswer)}
          >
            Yes
          </button>
          <button
            key="no"
            className="btn no-btn"
            onClick={() => onAnswer("No", author, correctAnswer)}
          >
            No
          </button>
        </div>
      );
    } else {
      if (options) {
        return (
          <div className="answer-multiple">
            {options.map((option, index) => (
              <button
                key={index}
                className="btn option-btn"
                onClick={() => onAnswer(option, correctAnswer, author)}
              >
                {option}
              </button>
            ))}
          </div>
        );
      }
    }
    return <div>buuuuu</div>;
  };

  return <div className="answer-container">{renderOptions()}</div>;
};

export default Answer;
