export interface QuizQuestionModel {
  id: number;
  quote: string;
  author: string;
  options: string[];
  correctAnswer: string;
}
