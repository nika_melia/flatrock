export interface QuestionModel {
  id: number;
  quote: string;
  author: string;
}
