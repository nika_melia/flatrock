import { QuestionModel } from "./models/QuestionsModel";
import { QuizQuestionModel } from "./models/QuizQuestionModel";

export const fetchQuestion = async (): Promise<QuestionModel[]> => {
  try {
    const response = await fetch("https://localhost:7107/api/Questions");
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    const questions: QuestionModel[] = await response.json();

    return questions;
  } catch (error) {
    console.error("There was a problem with the fetch operation:", error);
    return [];
  }
};

export const fetchNextQuestionById = async (
  id: number,
  mode: string
): Promise<QuizQuestionModel> => {
  const response = await fetch(
    `https://localhost:7107/api/Questions/GetRandomQuestion/${mode}/${id}`
  );
  if (!response.ok) {
    throw new Error("Network response was not ok");
  }
  const question: QuizQuestionModel = await response.json();

  return question;
};

export const fetchNextQuestion = async (
  mode: string
): Promise<QuizQuestionModel> => {
  const response = await fetch(
    `https://localhost:7107/api/Questions/GetRandomQuestion/${mode}`
  );
  if (!response.ok) {
    throw new Error("Network response was not ok");
  }
  const question: QuizQuestionModel = await response.json();
  return question;
};

// Function to add a new question
export const addQuestion = async (
  question: QuestionModel
): Promise<QuestionModel> => {
  const response = await fetch("https://localhost:7107/api/Questions", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify(question),
  });
  if (!response.ok) {
    throw new Error("Failed to add question");
  }
  return await response.json();
};

// Function to delete a question
export const deleteQuestion = async (id: number): Promise<void> => {
  const response = await fetch(`https://localhost:7107/api/Questions/${id}`, {
    method: "DELETE",
  });
  if (!response.ok) {
    throw new Error("Failed to delete question");
  }
};
